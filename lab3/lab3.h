#pragma once

#include <cstdio>

__device__ __host__ int CeilDiv(int a, int b);
__device__ __host__ int CeilAlign(int a, int b);

void PoissonImageCloning(
	const float *background,
	const float *target,
	const float *mask,
	float *output,
	const int wb, const int hb, const int wt, const int ht,
	const int oy, const int ox
);

#define CHECK {\
	auto e = cudaDeviceSynchronize();\
	if (e != cudaSuccess) {\
		printf("At " __FILE__ ":%d, %s\n", __LINE__, cudaGetErrorString(e));\
		abort(); \
	}\
}
