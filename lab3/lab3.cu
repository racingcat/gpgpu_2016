#include "lab3.h"
#define ITERATIONS 6000

__device__ static inline float AssignFixedHelper(
	const float *background, const float *target, const float *mask,
    const int wt, const int wb,
    const int yy, const int xx,
    const int y, const int x
    ){
    const int curt = wt*y+x;
    const int curb = wb*yy+xx;
    // Only add background weight if it is masked (black)
    // Minus is inverted in `CalculateFixedHelper`
    return mask[curt] < 127.0 ? target[3*curt] - background[3*curb] : target[3*curt];
}
__device__ static inline float CalculateFixedHelper(
	const float *background, const float *target, const float *mask,
	const int wb, const int hb, const int wt, const int ht,
	const int oy, const int ox,
	const int y, const int x
    ){

    float out = 0.0; 
    int cnt = 0;
    int yy = oy+y, xx = ox+x;

    // If current pixel is on boundary of whole target picture, but not the whole background, 
    // then blend the other side of boundary.

    // West
    if(x != 0){ cnt++; out += AssignFixedHelper(background, target, mask, wt, wb, yy, xx, y, x-1); }
    else if(xx > 0){   out -= background[3*(wb*yy+(xx-1))]; }
    // North
    if(y != 0){ cnt++; out += AssignFixedHelper(background, target, mask, wt, wb, yy, xx, y-1, x); }
    else if(yy > 0){   out -= background[3*(wb*(yy-1)+xx)]; }

    // East
    if(x != wt-1){ cnt++; out += AssignFixedHelper(background, target, mask, wt, wb, yy, xx, y, x+1); }
    else if(xx < wb-1){   out -= background[3*(wb*yy+(xx+1))]; }
    // South
    if(y != ht-1){ cnt++; out += AssignFixedHelper(background, target, mask, wt, wb, yy, xx, y+1, x); }
    else if(yy < hb-1){   out -= background[3*(wb*(yy+1)+xx)]; }

    return (float)cnt * target[3*(wt*y+x)] - out;
}
__global__ void CalculateFixed(
	const float *background, const float *target, const float *mask, float *fixed,
	const int wb, const int hb, const int wt, const int ht,
	const int oy, const int ox
){
	const int yt = blockIdx.y * blockDim.y + threadIdx.y;
	const int xt = blockIdx.x * blockDim.x + threadIdx.x;
    const int curt = wt*yt+xt;
	if (yt < ht && xt < wt && mask[curt] > 127.0f) {
		const int yb = oy+yt, xb = ox+xt;
		if (0 <= yb && yb < hb && 0 <= xb && xb < wb) {
            fixed[curt*3]   = CalculateFixedHelper(background,   target, mask,
                    wb, hb, wt, ht, oy, ox, yt, xt);
            fixed[curt*3+1] = CalculateFixedHelper(background+1, target+1, mask,
                    wb, hb, wt, ht, oy, ox, yt, xt);
            fixed[curt*3+2] = CalculateFixedHelper(background+2, target+2, mask,
                    wb, hb, wt, ht, oy, ox, yt, xt);
		}
	}
}

__device__ static inline float CalculateIterationHelper(
	const float *fixed, const float *mask, const float *target,
	const int y, const int x, const int wt, const int ht
){
    float out = 0.0; 

    // West
    if(x   != 0 && mask[wt*(y)+(x-1)] > 127.0) out += target[3*(wt*(y)+(x-1))];
    // East
    if(x+1 < wt && mask[wt*(y)+(x+1)] > 127.0) out += target[3*(wt*(y)+(x+1))];
    // North
    if(y   != 0 && mask[wt*(y-1)+(x)] > 127.0) out += target[3*(wt*(y-1)+(x))];
    // South
    if(y+1 < ht && mask[wt*(y+1)+(x)] > 127.0) out += target[3*(wt*(y+1)+(x))];

    return  (fixed[3*(wt*y+x)] + out) / 4.0;
}
__global__ void PoissonImageCloningIteration(
	const float *fixed, const float *mask, const float *target, float *output,
	const int wt, const int ht
){
	const int yt = blockIdx.y * blockDim.y + threadIdx.y;
	const int xt = blockIdx.x * blockDim.x + threadIdx.x;
    const int curt = wt*yt+xt;
	if (yt < ht && xt < wt && mask[curt] > 127.0f) {
        output[curt*3]   = CalculateIterationHelper(fixed,   mask, target,   yt, xt, wt, ht);
        output[curt*3+1] = CalculateIterationHelper(fixed+1, mask, target+1, yt, xt, wt, ht);
        output[curt*3+2] = CalculateIterationHelper(fixed+2, mask, target+2, yt, xt, wt, ht);
	}
}

__device__ __host__ int CeilDiv(int a, int b) { return (a-1)/b + 1; }
__device__ __host__ int CeilAlign(int a, int b) { return CeilDiv(a, b) * b; }

__global__ void SimpleClone(
	const float *background,
	const float *target,
	const float *mask,
	float *output,
	const int wb, const int hb, const int wt, const int ht,
	const int oy, const int ox
)
{
	const int yt = blockIdx.y * blockDim.y + threadIdx.y;
	const int xt = blockIdx.x * blockDim.x + threadIdx.x;
	const int curt = wt*yt+xt;
	if (yt < ht and xt < wt and mask[curt] > 127.0f) {
		const int yb = oy+yt, xb = ox+xt;
		const int curb = wb*yb+xb;
		if (0 <= yb and yb < hb and 0 <= xb and xb < wb) {
			output[curb*3+0] = target[curt*3+0];
			output[curb*3+1] = target[curt*3+1];
			output[curb*3+2] = target[curt*3+2];
		}
	}
}
void PoissonImageCloning(
    const float *background, const float *target, const float *mask, float *output,
    const int wb, const int hb, const int wt, const int ht,
    const int oy, const int ox
) {
#ifdef TIME
    float elapsed_time = 0.0;
    cudaEvent_t time_start, time_stop;
    cudaEventCreate(&time_start);
    cudaEventCreate(&time_stop);
#endif

    // set up
    dim3 gdim(CeilDiv(wt,32), CeilDiv(ht,16)), bdim(32,16);
    const int tsize = 3*wt*ht, bsize = 3*wb*hb;

    
    float *fixed, *buf1, *buf2;

    cudaMalloc(&fixed, tsize*sizeof(float));
    cudaMalloc(&buf1,  tsize*sizeof(float));
    cudaMalloc(&buf2,  tsize*sizeof(float));

#ifdef TIME
    cudaEventRecord(time_start);
#endif
    // initialize the iteration
    CalculateFixed<<<gdim, bdim>>>(
        background, target, mask, fixed,
        wb, hb, wt, ht, oy, ox
    );
#ifdef TIME
    cudaEventRecord(time_stop);
    cudaEventSynchronize(time_stop);
    cudaEventElapsedTime(&elapsed_time, time_start, time_stop);
    fprintf(stderr,"CalculateFixed elapsed (%f)\n", elapsed_time);
#endif

    CHECK;

    cudaMemcpy(buf1, target, sizeof(float)*tsize, cudaMemcpyDeviceToDevice);

#ifdef TIME
    cudaEventRecord(time_start);
#endif
    // iterate
    for (int i = 0; i < ITERATIONS; ++i) {
        PoissonImageCloningIteration<<<gdim, bdim>>>(
            fixed, mask, buf1, buf2, wt, ht
        );
        PoissonImageCloningIteration<<<gdim, bdim>>>(
            fixed, mask, buf2, buf1, wt, ht
        );
    }
#ifdef TIME
    cudaEventRecord(time_stop);
    cudaEventSynchronize(time_stop);
    cudaEventElapsedTime(&elapsed_time, time_start, time_stop);
    fprintf(stderr,"Iterations elapsed (%f)\n", elapsed_time);
#endif

    CHECK;

    // copy the image back
    cudaMemcpy(output, background, bsize*sizeof(float), cudaMemcpyDeviceToDevice);
    SimpleClone<<<gdim, bdim>>>(
        background, buf1, mask, output,
        wb, hb, wt, ht, oy, ox
    );

    // clean up
    cudaFree(fixed);
    cudaFree(buf1);
    cudaFree(buf2);
}
