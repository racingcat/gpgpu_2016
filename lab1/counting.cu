#include "counting.h"
#include "SyncedMemory.h"
#include <cstdio>
#include <cassert>
#include <cmath>
#include <vector>
#include <thrust/copy.h>
#include <thrust/scan.h>
#include <thrust/for_each.h>
#include <thrust/transform.h>
#include <thrust/functional.h>
#include <thrust/device_ptr.h>
#include <thrust/execution_policy.h>
#include <thrust/device_new.h>
#include <thrust/device_delete.h>
#define blockThreads 256
#define blockGridWidth 160
#define blockGridHeight(in) ((in / blockThreads) / blockGridWidth)

#define CHECK {\
	auto e = cudaDeviceSynchronize();\
	if (e != cudaSuccess) {\
		printf("At " __FILE__ ":%d, %s\n", __LINE__, cudaGetErrorString(e));\
		/* abort(); */ \
	}\
}
void printText(const char *text, int text_size){
    /* Move GPU memory to host */
    auto text_host = new char[text_size];
    cudaMemcpy(text_host, text, sizeof(char)*text_size, cudaMemcpyDeviceToHost);
    for(int i = 0 ; i < text_size; ++i ){
        if( text_host[i] == '\n' )
            putchar('_');
        else
            printf("%c", text_host[i]);
    }
    puts("");
    delete[] text_host;
}
__device__ __host__ int CeilDiv(int a, int b) { return (a-1)/b + 1; }
__device__ __host__ int CeilAlign(int a, int b) { return CeilDiv(a, b) * b; }

__global__ void transformTextKernel( const char* text, bool* type, int text_size ){
    int idx = threadIdx.x + blockIdx.x * blockDim.x; 
    if( idx < text_size )
        type[idx] = ('\n' != text[idx]); 
}

__global__ void buildSegmentTree( bool pre_segment_tree[], bool cur_segment_tree[], int segment_cnt ){
    int idx = threadIdx.x + blockIdx.x * blockDim.x; 
    if( idx < segment_cnt )
        cur_segment_tree[idx] = (pre_segment_tree[ idx << 1 ] && pre_segment_tree[ (idx << 1) + 1 ]);
}
__global__ void buildPos( bool* segment_trees[], int pos[], int text_size ){
    int idx = threadIdx.x + blockIdx.x * blockDim.x; 
    int cnt = 0;

    if( idx < text_size ){
        if(segment_trees[0][idx] ){
            int counter = 0;
            while( idx >= 0 && segment_trees[counter][idx] ){
                if( idx % 2 == 0 ){
                    cnt += (1<<counter); 
                }
                idx = (idx-1)>>1;
                counter++;
            }

            while( idx >= 0 && counter >= 0 ){
                if( segment_trees[counter][idx] ){
                    cnt += (1<<counter); 
                    idx = idx-1;
                }

                idx = (idx<<1) + 1;
                counter--;
            }


        }
        pos[threadIdx.x + blockIdx.x * blockDim.x] = cnt;
    }
}
void CountPosition(const char *text, int *pos, int text_size)
{
    // see https://goo.gl/CQePMo  for cudaOccupancyMaxPotentialBlockSize
     
    int block_size = 256; 
    int grid_size = CeilDiv(text_size, block_size); 
    int buffer_size = 2 + log2((double)text_size);
    // int grid_size = ( text_size + block_size - 1) / block_size; 

    MemoryBuffer<bool*> segment_trees_buffer(buffer_size);
    auto segment_trees_sync = segment_trees_buffer.CreateSync(buffer_size);
    auto segment_trees = segment_trees_sync.get_cpu_wo();
    int iteration = 0, segment_cnt = text_size; 

    cudaMalloc(&(segment_trees[iteration]), sizeof(bool) * segment_cnt);
    transformTextKernel<<< grid_size, block_size >>>( text, segment_trees[iteration], text_size );
    cudaDeviceSynchronize();

    // Divide segment by 2 in every iteration
    while( segment_cnt >>= 1 ){ 
        iteration++;
        grid_size = CeilDiv(grid_size,2);

        cudaMalloc(&(segment_trees[iteration]), sizeof(bool) * segment_cnt);
        buildSegmentTree<<< grid_size, block_size >>>( segment_trees[iteration-1], segment_trees[iteration], segment_cnt );
        cudaDeviceSynchronize();
    }

    buildPos<<< CeilDiv(text_size, block_size), block_size >>>( segment_trees_sync.get_gpu_rw(), pos ,text_size );

    /* CHECK; */
    cudaDeviceSynchronize();
}

struct is_one
{
    __host__ __device__
    bool operator()(int x)
    {
        return x == 1;
    }
};
int ExtractHead(const int *pos, int *head, int text_size)
{
    int *buffer;
    cudaMalloc(&buffer, sizeof(int)*text_size*2); // this is enough
    thrust::device_ptr<const int> pos_d(pos);
    thrust::device_ptr<int> head_d(head), flag_d(buffer), cumsum_d(buffer+text_size);

    thrust::sequence(flag_d, flag_d + text_size);

    /*
       Copy the index in sequence array which has '1' in corresponding pos array

       Return(last_elem): pointer to last element of head_d array

    */
    auto last_elem = thrust::copy_if(thrust::device, flag_d, flag_d + text_size, pos_d, head_d, is_one());
	cudaFree(buffer);

    return last_elem - head_d;
}

#define MARKER 'Z'
#define SPACE '\n'

struct add_marker{

    char *text;

    __host__ __device__
    void operator()(int idx){
        if( idx != 0 )
            text[idx-1] = MARKER;
    }

    __host__ __device__
    add_marker(char* _text){
        text = _text;
    }
};
struct trim_space{
    __host__ __device__
    bool operator()(const char x){
        return x != SPACE;
    }
};
void Part3(char *text, int *pos, int *head, int text_size, int n_head){
/*
    Objective: Trim extra whitespaces
    Example input:  there__is____lots____of_useless___________whitespace
    Example output: there_is_lots_of_useless_whitespace
*/
    thrust::device_ptr<const int> head_v(head); 
    thrust::device_ptr<char> text_v(text);

    /* Replace the last character of every space region with a dummy marker */
    thrust::for_each_n(thrust::device, head_v, n_head, add_marker(text));

    /* Prepare a temporary buffer for copy_if */
    thrust::device_ptr<char> tmp = thrust::device_new<char>(text_size);
    thrust::copy(thrust::device, text_v, text_v+text_size, tmp);

    /* Trim all space */
    auto text_trimmed = thrust::copy_if(thrust::device, tmp, tmp + text_size, text_v, trim_space());

    /* Restore spaces previously substituted by markers */
    thrust::replace(thrust::device, text_v, text_trimmed, MARKER, SPACE);

    /* Free the temporary space */
    thrust::device_delete<char>(tmp, text_size);

    /* Print the result */
    printText(text, text_trimmed - text_v);
}
