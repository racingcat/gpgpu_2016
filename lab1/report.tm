<TeXmacs|1.0.7.18>

<style|article>

<\body>
  <doc-data|<doc-title|GPGPU 2016 Spring>|<\doc-author>
    Homework 1 Report

    \;

    Yun-Chih Chen

    b03902074@ntu.edu.tw
  </doc-author>>

  <section*|Trimming extra whitespaces>

  <paragraph|Example input:><verbatim|there__is____lots____of_useless___________whitespace>

  <paragraph|Example output:><verbatim|there_is_lots_of_useless_whitespace>

  \;

  <section*|How I do that>

  The following steps are implemented purely in
  <with|font-shape|italic|thrust>.

  \;

  1. For each index <verbatim|idx> in the <verbatim|head> array, mark the
  whitespace preceding it in <verbatim|text> by an arbitrary character
  <math|c>, i.e. <verbatim|text[idx-1] = c>.

  2. Make a copy of the <verbatim|text> array, <verbatim|tmp>.

  3. Remove all whitespace character in <verbatim|tmp> and write the result
  back to <verbatim|text>. \ Note that characters marked as <math|c> will not
  be removed.

  4. Replace all characters marked by <math|c> back to whitespace.

  \;

  In this way, clamps of whitespaces are effectively trimmed.

  \;

  <section*|Code>

  <\cpp-code>
    /* Replace the last character of every space region with a dummy marker
    */

    thrust::for_each_n(thrust::device, head_v, n_head, add_marker(text));

    \;

    /* Prepare a temporary buffer for copy_if */

    thrust::device_ptr\<char\> tmp = thrust::device_new\<char\>(text_size);

    thrust::copy(thrust::device, text_v, text_v+text_size, tmp);

    \;

    /* Trim all space */

    auto text_trimmed = thrust::copy_if(thrust::device,\ 

    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ tmp, tmp + text_size, text_v,
    trim_space());

    \;

    /* Restore spaces previously substituted by markers */

    thrust::replace(thrust::device, text_v, text_trimmed, MARKER, SPACE);

    \;

    /* Free the temporary space */

    thrust::device_delete\<char\>(tmp, text_size);

    \;

    /* Print the result */

    printText(text, text_trimmed - text_v);

    \;

    \;
  </cpp-code>
</body>

<initial|<\collection>
</collection>>

<\references>
  <\collection>
    <associate|auto-1|<tuple|?|?>>
    <associate|auto-2|<tuple|1|?>>
    <associate|auto-3|<tuple|2|?>>
    <associate|auto-4|<tuple|2|?>>
    <associate|auto-5|<tuple|2|?>>
  </collection>
</references>