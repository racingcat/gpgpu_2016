#include "lab2.h"
#include "maze.h"

struct Lab2VideoGenerator::Impl {
	int t = 0;
};

Lab2VideoGenerator::Lab2VideoGenerator(): impl(new Impl) {
}

Lab2VideoGenerator::~Lab2VideoGenerator() {}

void Lab2VideoGenerator::get_info(Lab2VideoInfo &info) {
	info.w = W;
	info.h = H;
	info.n_frame = NFRAME;
	// fps = 24/1 = 24
    info.fps_n = 24;
	info.fps_d = 1;
};

void Lab2VideoGenerator::Generate(uint8_t *yuv) {
    static Maze maze;
    // Draw a wall
    if( impl->t % 2 == 0 )
        maze.make_wall();
    // Make a wall
    else
        maze.make_hole();
    
    if( impl->t % 20 == 0 )
        maze.initialize_rand();

    maze.draw_background(yuv, impl->t);
    maze.draw_line(yuv, impl->t);

	++(impl->t);
}
