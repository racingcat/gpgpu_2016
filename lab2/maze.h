#pragma once
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <cmath>
#include <vector>
#include <utility>
#include <cstdint>
#include <curand_kernel.h>
#include <thrust/copy.h>
#include <thrust/replace.h>
#include <thrust/functional.h>
#include <thrust/device_ptr.h>
#include <thrust/device_new.h>
#include <thrust/device_delete.h>
#include <thrust/execution_policy.h>

struct Point {
    int x,y;
    Point(int _x = 0, int _y = 0): x(_x), y(_y){}
};

struct Box {
    Point top, bottom; 

    Box(int xt, int yt, int xb, int yb): top(xt,yt), bottom(xb,yb){}
    Box(Point t, Point b): top(t), bottom(b){}
    Box(){}
    bool too_small();
    std::pair<Point,Point> split();
};

class Maze {

public:
        
    Maze();
    ~Maze();
    const uint8_t* get_maze();
    bool next_box(Box& box);
    void make_hole();
    void make_wall();
    void draw_background(uint8_t* yuv, int t);
    void draw_line(uint8_t* yuv, unsigned int t);
    void print_maze();
    void initialize_rand();

private:
    void put_color(uint8_t* yuv, unsigned int t);
    void make_line();

    std::vector<Box> boxes;
    // Number of grids in x,y coordinate
    int x_max, y_max;
    // The drawing buffer
    uint8_t* maze;
    thrust::device_ptr<uint8_t> maze_dev;
    curandState *rand_state; 

    std::pair<Point,Point> wall;
};

// +---+----------------------------+   +------------+
// |   |                            |                |
// |   |                            |  line_width    |
// +--------------------------------+                |
// |   |                            |                |
// |   |                            |                |
// |   |                            |                |
// |   |                            |                |
// |   |                            |                |
// |   |                            |                +--->  grid_width
// |   |                            |                |
// |   |                            |                |
// |   |                            |                |
// |   |                            |                |
// |   |                            |                |
// |   |                            |                |
// |   |                            |                |
// |   |                            |                |
// +---+----------------------------+   +------------>
