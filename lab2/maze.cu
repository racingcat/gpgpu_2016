#include "maze.h"
#include "lab2.h"
#define PI 3.141592 
#define blockGridHeight(in) ((in / blockThreads) / blockGridWidth)
#define CHECK {\
	auto e = cudaDeviceSynchronize();\
	if (e != cudaSuccess) {\
		printf("At " __FILE__ ":%d, %s\n", __LINE__, cudaGetErrorString(e));\
		abort(); \
	}\
}
__global__ static void put_maze_Y(curandState * state, uint8_t* u, unsigned int t);
__global__ static void put_maze_U(curandState * state, uint8_t* u, unsigned int t);
__global__ static void put_maze_V(curandState * state, uint8_t* v, unsigned int t);
__global__ static void setup_rand(curandState * state, unsigned long seed);

bool Box::too_small(){
    return bottom.x - top.x <= 1 || bottom.y - top.y <= 1;
}
std::pair<Point,Point> Box::split(){
    // If width > height, split vertically
    if( bottom.x - top.x > bottom.y - top.y){
        // Vertical split: pick a random point between bottom.x and top.x
        int x = (rand() % (bottom.x - top.x - 1)) + top.x + 1;
        return std::make_pair(
                    Point(x,bottom.y),
                    Point(x,top.y)
                );
    }
    else{
        // Horizontal split: pick a random point between bottom.y and top.y
        int y = (rand() % (bottom.y - top.y - 1)) + top.y + 1;
        return std::make_pair(
                    Point(bottom.x, y),
                    Point(top.x, y)
                );
    }
}
Maze::Maze(){
    x_max = W/grid_size;
    y_max = H/grid_size;
    std::srand(std::time(0));
    boxes.push_back(Box(0,0,x_max,y_max));

    // Allocate space for CPU/GPU maze
    maze = new uint8_t[W*H];
    maze_dev = thrust::device_new<uint8_t>(W*H);
    memset(maze, 0, W*H);

    // Allocate space for random number generator
    cudaMalloc( &rand_state, W*H/4*sizeof(curandState) );
}
Maze::~Maze(){
    delete[] maze;
    thrust::device_delete<uint8_t>(maze_dev, W*H);
}
const uint8_t* Maze::get_maze(){
    return maze;
}

bool Maze::next_box(Box& box){
    if( boxes.empty() )
        return false;

    while(boxes.back().too_small()){
        boxes.pop_back();
        if( boxes.empty() )
            return false;
    }
    box = boxes.back();
    boxes.pop_back();
    return true;
}
void Maze::make_wall(){
    Box box;
    if(!next_box(box))
        return;

    wall = box.split();
    make_line();
    boxes.push_back( Box(box.top, wall.first) );
    boxes.push_back( Box(wall.second, box.bottom) );
}
void Maze::make_hole(){
    if( wall.first.x == wall.second.x ){
        // Draw vertical hole
        // wall.first: bottom point
        // wall.second: top point
        int y = rand() % (wall.first.y-wall.second.y) + wall.second.y;

        int until = grid_size*(wall.first.x + (y+1)*W);
        for( int id = grid_size*(wall.first.x + y*W) + W*line_width; id < until; id += W ){
            for( int j = 0; j < line_width; j++ ){
                maze[id+j] = 0;
            }
        }
    }
    else{
        // Draw horizontal hole
        // wall.first: right point
        // wall.second: left point
        int x = rand() % (wall.first.x-wall.second.x) + wall.second.x;

        uint8_t* pos = maze + grid_size*(x + wall.second.y*W) + line_width;
        for(int j = 0; j < line_width; j++)
            memset( pos + j*W, 0, grid_size - line_width );
    }
}
void Maze::make_line(){
    if( wall.first.x == wall.second.x ){
        // Draw vertical line
        // wall.first: bottom point
        // wall.second: top point
        int until = grid_size*(wall.first.x + wall.first.y*W);
        #pragma omp parallel for
        for( int id = grid_size*(wall.first.x + wall.second.y*W); id < until; id += W ){
            for( int j = 0; j < line_width; j++ ){
                maze[id+j] = 1;
            }
        }
    }
    else{
        // Draw horizontal line
        // wall.first: right point
        // wall.second: left point
        uint8_t* pos = maze + grid_size*(wall.second.x + wall.second.y*W);
        int len = grid_size*(wall.first.x - wall.second.x);
        for(int j = 0; j < line_width; j++)
            memset( pos + j*W, 1, len );
    }

}
void Maze::draw_background(uint8_t* yuv, int t){

    // Launch W*H/4 threads: grid_size((W/2)/8,(W/2)/8)
    dim3 block_size(8, 8); 
    dim3 grid_size(W/block_size.x, H/block_size.y); 
    put_maze_Y<<<grid_size, block_size>>>(rand_state, yuv, t);
    CHECK;
}
void Maze::draw_line(uint8_t* yuv, unsigned int t){
    int color = (float)line_color_y * (cospif(t/(float)NFRAME*5.0)/10.0+0.1);
    static thrust::identity<int> identity;
    thrust::device_ptr<uint8_t> yuv_v(yuv);
    thrust::copy(maze, maze+W*H, maze_dev);
    thrust::replace_if(thrust::device, yuv_v, yuv_v + W*H, maze_dev, identity, color);
    put_color(yuv, t);
}
void Maze::initialize_rand(){
    time_t now;
    time(&now);
    dim3 block_size(8, 8); 
    dim3 grid_size((W/2)/block_size.x, (H/2)/block_size.y); 
    setup_rand<<<grid_size, block_size>>>(rand_state, (unsigned long)now);
}
void Maze::put_color(uint8_t* yuv, unsigned int t){
    // Launch W*H/4 threads: grid_size((W/2)/8,(W/2)/8)
    dim3 block_size(8, 8); 
    dim3 grid_size((W/2)/block_size.x, (H/2)/block_size.y); 

    put_maze_U<<<grid_size, block_size>>>(rand_state, yuv + W*H, t);
    put_maze_V<<<grid_size, block_size>>>(rand_state, yuv + W*H + W*H/4, t);
    CHECK;
}
__global__ static void put_maze_Y(curandState * state, uint8_t* u, unsigned int t){
    unsigned int x = (blockIdx.x * blockDim.x) + threadIdx.x;
    unsigned int y = (blockIdx.y * blockDim.y) + threadIdx.y;
    unsigned int id = x + W*y; 
    float tt = (float)t/(float)NFRAME;
    float ratio = (4.0 + sinpif(tt-0.63) + 7.0*cospif((float)tt/3.0-0.77) +
            5.0*cosf(3.0*(float)x+1.2*tt*(float)y) )/14.0 ;

    if( id < W*H )
        u[id] = (int)(ratio*90.0) + 100;
}
__global__ static void put_maze_U(curandState * state, uint8_t* u, unsigned int t){
    unsigned int x = (blockIdx.x * blockDim.x) + threadIdx.x;
    unsigned int y = (blockIdx.y * blockDim.y) + threadIdx.y;
    unsigned int id = x + W*y/2; 

    float tt = (float)t/(float)NFRAME*20.0;
    float ratio = 1.0 - sinpif(tt)/5.0;
    if( id < W*H/4 ){
        u[id] = (int)(x*400.0*ratio/W);
        if( u[id] < 40.0*tt )
            u[id] += 10.0*tt*ratio;
    }
}
__global__ static void put_maze_V(curandState * state, uint8_t* v, unsigned int t){
    unsigned int x = (blockIdx.x * blockDim.x) + threadIdx.x;
    unsigned int y = (blockIdx.y * blockDim.y) + threadIdx.y;
    int id = x + W*y/2;

    float tt = (float)t/(float)NFRAME;
    float ratio = 1.0 - cospif(tt*15.0)/5.0;
    if( id < W*H/4 ){
        v[id] = (int)(y*400.0*ratio/H);
        if( v[id] < 100.0*tt )
            v[id] += 150.0*tt*ratio;
    }
}
__global__ static void setup_rand(curandState * state, unsigned long seed)
{
    unsigned int x = (blockIdx.x * blockDim.x) + threadIdx.x;
    unsigned int y = (blockIdx.y * blockDim.y) + threadIdx.y;
    unsigned int id = x + W*y/2;
    curand_init ( seed, id , id, &state[id] );
}
