#pragma once
#include <cstdint>
#include <memory>
using std::unique_ptr;

// Size of the whole maze
const unsigned W = 640;
const unsigned H = 480;

// Line width, grid width
const unsigned grid_size = 16;
const unsigned line_width = 2;

const unsigned NFRAME = 1100;

const unsigned line_color_y = 254;

struct Lab2VideoInfo {
	unsigned w, h, n_frame;
	unsigned fps_n, fps_d;
};

class Lab2VideoGenerator {
	struct Impl;
	unique_ptr<Impl> impl;
public:
	Lab2VideoGenerator();
	~Lab2VideoGenerator();
	void get_info(Lab2VideoInfo &info);
	void Generate(uint8_t *yuv);
};
